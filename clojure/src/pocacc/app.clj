(ns pocacc.app
  (:require
   [pocacc.utils :as utils])
  (:require [pocacc.core :as core])
  (:require [pocacc.wallet :as w])
  (:require [pocacc.sportsbook :as sb])
  (:require [pocacc.markets :as m])
  ;; (:require [clojure.core.match :refer [match]])
  
  (:use [pocacc.wallet])
  (:use [slingshot.slingshot :only [throw+ try+]])
  (:use [co.paralleluniverse.pulsar.core])

  (:gen-class))

(comment "
  TODO
  ----

  complete bet placement use case
  clustering with pulsar / galaxy?
  persistence
  unit tests??

  https://clojuredocs.org/clojure.core/set-validator!
  https://github.com/puniverse/pulsar
  http://docs.paralleluniverse.co/pulsar/
  http://codeofrob.com/entries/the-leveldb-storage-for-my-clojure-document-database.html
  http://www.axonframework.org/docs/2.0/sagas.html
")

;; test

(w/create-wallet-command "1234")
(w/deposit-command "1234" 50)
(w/deposit-command "1234" 100)
(w/withdraw-command "1234" 1 -)
(w/withdraw-command "1234" 1000 -)

(m/create-market-command "1" "Rich vs Steve 1X2")
(m/create-selection-command "1" "1" "Rich")
(m/create-selection-command "1" "2" "Draw")
(m/create-selection-command "1" "3" "Steve")

(m/create-market-command "2" "Tom vs Kevin 1X2")
(m/create-selection-command "2" "1" "Tom")
(m/create-selection-command "2" "2" "Draw")
(m/create-selection-command "2" "3" "Kevin")

;;;;; TODO make selections 'do stuff'
;; bet-id, account-id, selections, stake
(sb/place-bet-command "99" "1234" [{ :market "1" :selection "1" }
                                   { :market "2" :selection "1" }] 10)

(m/result-market "1" ["1"] ["2", "3"])

(comment
  ;; Reset
  (swap! core/event-store (fn [x] (hash-map)))
  (swap! core/listeners (fn [x] (hash-map)))

  ;; fiddle
  @core/event-store
  @core/listeners
  (core/remove-listeners! -)

  ;; benchmarking
  (try+
   (w/create-wallet-command "999")
   (dotimes [n 1000]
     (fiber (w/deposit-command "999" 50)))
   (catch Object _
     (print "exception")))

  (time (core/rehydrate - "999"))
  )
