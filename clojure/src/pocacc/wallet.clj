(ns pocacc.wallet
  (:require [pocacc.utils :as utils])
  (:use [pocacc.core]))

;; events
(defrecord wallet-created [id])
(defrecord funds-deposited [id amount])
(defrecord funds-withdrawn [id amount external-ref])

;; aggregate
(defrecord wallet [id transactions balance])

;; entity
(defrecord transaction [amount])

;; operations
(defn create-wallet [id]
  (raise nil id (->wallet-created id)))

(defn deposit [aggregate id amount]
  ;; todo business logic
  ;; raise funds-deposited event
  (print ".")
  (raise aggregate id (->funds-deposited id amount)))

(defn withdraw [aggregate id amount external-ref]
  "Only allow withdrawal where balance would still be >= 0"
  (if (<= 0 (- (:balance aggregate) amount))
    (raise aggregate id (->funds-withdrawn id amount external-ref))
    :insufficient-funds))

;;
;; commands & handlers
;;
(defn create-wallet-command [id]
  (create-wallet id))

(defn deposit-command [id amount]
  (utils/try-times
   1
   (let [aggregate (rehydrate wallet id)]
     (deposit aggregate id amount))))

(defn withdraw-command [id amount external-ref]
  (let [aggregate (rehydrate wallet id)]
    (withdraw aggregate id amount external-ref)))

;;
;; event handlers
;;
(defmethod apply-event wallet-created [- event]
  (->wallet (:id event) [] 0))

(defmethod apply-event funds-deposited [aggregate event]  
  (assoc aggregate
         :transactions (conj (:transactions aggregate) (->transaction (:amount event)))
         :balance (+ (:balance aggregate) (:amount event))))

(defmethod apply-event funds-withdrawn [aggregate event]  
  (assoc aggregate
         :transactions (conj (:transactions aggregate) (->transaction (:amount event)))
         :balance (- (:balance aggregate) (:amount event))))
