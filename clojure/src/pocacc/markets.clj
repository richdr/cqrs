(ns pocacc.markets
  (:require [pocacc.utils :as utils])
  (:use [pocacc.core]))

;; events
(defrecord market-created [id name selections])
(defrecord selection-created [market-id id name])
(defrecord selection-resulted-as-winner [market-id id])
(defrecord selection-resulted-as-loser [market-id id])

;; aggregate
(defrecord market [id name selections])

;; entities
(defrecord selection [id name])

;; operations
(defn create-market [id name]
  (raise nil id (->market-created id name [])))

(defn create-selection [aggregate id name]
  (raise aggregate (:id aggregate) (->selection-created (:id aggregate) id name)))

(defn result-market [aggregate winners losers]
  (doseq [selection-id winners]
    (raise aggregate (:id aggregate) (->selection-resulted-as-winner (:id aggregate) selection-id)))
  (doseq [selection-id winners]
    (raise aggregate (:id aggregate) (->selection-resulted-as-loser (:id aggregate) selection-id))))

;; commands
(defn create-market-command [id name]
  (create-market id name))

(defn create-selection-command [market-id id name]
  (let [aggregate (rehydrate market market-id)]
    ;; todo check there is no matching named selection
    (create-selection aggregate id name)))

(defn result-market [id winners losers]
  (let [aggregate (rehydrate market id)]
    (result-market aggregate winners losers)))

;; event handlers
(defmethod apply-event market-created [- event]
  (->market (:id event) (:name event) (:selections event)))

(defmethod apply-event selection-created [aggregate event]
  (assoc aggregate
         :selections (conj (:selections aggregate)
                           (->selection (:id event) (:name event)))))

(defmethod apply-event selection-resulted-as-loser [aggregate event] )
(defmethod apply-event selection-resulted-as-winner [aggregate event] )
