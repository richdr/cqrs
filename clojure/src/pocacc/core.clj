(ns pocacc.core
  (:require
;   [redis-async.core :as redis-async]
;   [redis-async.client :as client]
;   [clojure.core.async :as async]
   [pocacc.utils :as utils])
  (:use [slingshot.slingshot :only [throw+ try+]])
  (:use [co.paralleluniverse.pulsar.core])
  (:gen-class))

;(def p (redis-async/make-pool {})) 
;(<!! (client/set p "KEY1" "VALUE-A"))
;(<!! (client/get p "KEY1"))

;; core
(defmulti apply-event (fn [agg event] (class event)))

(def event-store (atom (hash-map)))
(defn events-for-aggregate [id]
  (or (get (deref event-store) id) []))

(defn store [agg-id event expected-version]
  "store an event against an aggregate at a particular version"
  (swap! event-store
         (fn [current]
           (if
               (or                
                (= expected-version (count (events-for-aggregate agg-id)))
                (= expected-version 'ignore-version)
                (= expected-version 'nil))
             (assoc current
                    agg-id
                    (conj (events-for-aggregate agg-id) event))
             (throw+ 'concurrency-exception)))))

(defn rehydrate [type id]
  "given an aggregate ID and type, return the rehydrated aggregate"
  (let [events (events-for-aggregate id)]
    (print "Rehydrating with " (count events) " events\n")
    (assoc (reduce apply-event nil events) :version (count events))))

(def listeners (atom (vector)))
(defn listen! [event f owner]
  "add a listener to the vector of listeners"
  (swap! listeners
         (fn [current]
           (conj current {:function f :owner owner :event event}))))

(defn listeners-for [event]
  "find relevant listeners for an event"
  (filter (fn [listener] (= event (:event listener)))
          @listeners))

;; TODO not working?
(defn remove-listeners! [owner]
  (swap! listeners
         (fn [current]
           (filter (fn [listener] (not (= owner (:owner listener))))
                   current))))

(defn publish-event [event]
  "fire event into relevant listeners"
  (doseq [listener (listeners-for (type event))]
    (apply (:function listener) [event])))

;;;; parallel version - doesn't work?
;; (defn publish-event [event]
;;   (doseq [f (map
;;              (fn [listener]
;;                (fiber (apply (:function listener) [event])))
;;              (listeners-for (type event)))]
;;     (join f)))
  
(defn raise [aggregate agg-id event]
  ;; what's the safest/correct order for these steps?
  ;; todo failure scenarios
  (store agg-id event (:version aggregate))
  (publish-event event)
  (apply-event aggregate event))
