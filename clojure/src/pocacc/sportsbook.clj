(ns pocacc.sportsbook
  (:require [pocacc.utils :as utils])
  (:require [pocacc.wallet :as w])
  (:import [pocacc.wallet funds-withdrawn])
  (:use [pocacc.core]))

;; events
(defrecord bet-placement-requested [id account-id selections stake])
(defrecord bet-placed [id])

;; aggregate
(defrecord bet [id account-id selections stake status])

;; operations
(defn place-bet [id account-id selections stake]
  (raise nil id (->bet-placement-requested id account-id selections stake)))

(defn confirm-bet [id]
  (raise nil id (->bet-placed id)))

;; commands
(defn place-bet-command [id account-id selections stake]
  (place-bet id account-id selections stake))

(defn confirm-placement-command [id]
  (confirm-bet id))

;; event handlers
(defmethod apply-event bet-placement-requested [- event]
  (->bet (:id event) (:account-id event) (:selections event) (:stake event) :pending))

(defmethod apply-event bet-placed [aggregate event]  
  (assoc aggregate :status :placed))


;;
;; Bet Placement
;;

;; TODO add way to persist saga state hash
(defn placement-process-funds-withdrawn [event]
  (print "3. funds withdrawn, confirming bet\n")
  (confirm-placement-command (:id event))
  (remove-listeners! :placement-process))

;; TODO add periodic firing to removed failed/timed out processes
;;  and cancel the bet and/or withdraw
(defn placement-process-bet-placement-requested [event]
  (print "1. placement process started\n")
  (listen! funds-withdrawn (fn [e]
                             (print "2. event fired, does it match?\n")
                             (if (= (:id event) (:external-ref e))
                               (placement-process-funds-withdrawn event)))
           :placement-process)
  (w/withdraw-command (:account-id event) (:stake event) (:id event)))

(listen! bet-placement-requested placement-process-bet-placement-requested nil)
(listen! bet-placed (fn [x] (print "\n" x "4. fully placed\n")) -)


;;
;; Bet Settlement
;;

(listen! selection-resulted-as-loser (partial selection-resulted 'win))
