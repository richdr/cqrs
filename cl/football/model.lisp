(ql:quickload "cells")  ;;; https://github.com/kennytilton/cells
(in-package :cells)

(defun factorial (n)
  (reduce #'* (loop for i from 1 to n collect i)))

(defun poisson (goals expectancy)
  (/ (* (expt expectancy goals) (exp (* -1 expectancy))) (factorial goals)))

(defun winner (h a)
  (cond ((= h a) 'draw)
		((> h a) 'home)
		((< h a) 'away)))

(defmd match ()
  (supremacy)
  (total-goals)
  (home-expected (c? (+ (^supremacy) (/ (^total-goals) 2))))
  (away-expected (c? (/ (^total-goals) 2))))

(defmd correct-score-outcome ()
  (match)
  (home-score)
  (away-score)
  (probability (c? (* (poisson (^home-score) (home-expected (^match)))
					  (poisson (^away-score) (away-expected (^match))))))
  (winning-side))

(defun sum-scoreline-outcomes-by-side (scorelines side)
	   (reduce #'+
			   (mapcar
				#'(lambda (scoreline)
					(if (eq side (winning-side scoreline)) (probability scoreline) 0))
				scorelines)))

(defmd 1x2-market ()
  (score-outcomes)
  (home-probability (c?  (sum-scoreline-outcomes-by-side (^score-outcomes) 'home)))
  (draw-probability (c?  (sum-scoreline-outcomes-by-side (^score-outcomes) 'draw)))
  (away-probability (c?  (sum-scoreline-outcomes-by-side (^score-outcomes) 'away))))

(defobserver home-probability ((self 1x2-market))
  (trc "1x2 home" new-value))

(defobserver draw-probability ((self 1x2-market))
  (trc "1x2 draw" new-value))

(defobserver away-probability ((self 1x2-market))
  (trc "1x2 away" new-value))

(let* ((match (make-instance 'match :total-goals (c-in 2.4) :supremacy (c-in -0.35) ))
	   (scorelines (loop for home from 0 to 8
					  nconc (loop for away from 0 to 8
							   collect (make-instance 'correct-score-outcome
													  :match match
													  :home-score home :away-score away
													  :winning-side (winner home away))))))
  (make-instance '1x2-market :score-outcomes scorelines))

;; (setf (supremacy match) 2)

;; (defobserver home-expected ()
;;  (trc "home expected" new-value old-value))
;; (defobserver away-expected ()
;;   (trc "away expected" new-value old-value))
;; (defobserver probability ((self correct-score-outcome))
;;   (trc "probability" new-value old-value (home-score self) (away-score self)))
