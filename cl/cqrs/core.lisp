(defgeneric apply-event (event aggregate))

(defmacro with-retries (number &rest body)
  "retry `number' times to run body, or until it doesn't throw a 'versioning-fail"
  `(if (dotimes (c ,number)
         (let ((v (catch 'versioning-fail ,@body)))
           (unless (eq v 'nil)
             (return 'true))))
       'true 'false))

(defun raise (aggregate agg-id event version)
  (store-event agg-id event version)
  (publish-event event)
  (apply-event event aggregate))

(defun publish-event (event)
  (listener event))

(defun store-event (agg-id event expected)
  (redis:with-connection ()
    (red:watch agg-id)
    (let ((current (red:llen agg-id)))
      (if (= current expected)
          (progn
            (red:multi)
            (red:lpush agg-id (print event (make-broadcast-stream)))
            (red:exec)
            current)
          (throw 'versioning-fail 'nil)
          ))))

(defun rehydrate (class id)
  (redis:with-connection ()
    (let* ((events (reverse (red:lrange id 0 -1)))
           (aggregate (make-instance class :id id))
           )
      (dolist (x events)        
        (setf aggregate (apply-event (read-from-string x) aggregate)))
      (values (length events) aggregate))))
