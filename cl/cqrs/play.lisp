(ql:quickload "uuid")
(ql:quickload "cl-redis")
(ql:quickload "bordeaux-threads") ;; https://trac.common-lisp.net/bordeaux-threads/wiki/ApiDocumentation
;; https://github.com/gregoryyoung/m-r/blob/1a769bbd24bfbd037e2e77cbb8aba6e2a294045f/SimpleCQRS/Domain.cs

(load "serialize.lisp")
(load "core.lisp")

;; Commands

(defun create-ticketed-event-command (id name tickets)
  (catch 'versioning-fail
    (create-ticketed-event id name tickets)
    'nil))

(defun change-capacity-command (id tickets)
  (with-retries 2
    (multiple-value-bind (version aggregate)
        (rehydrate 'ticketed-event id)
      (change-capacity aggregate tickets version))))

(defun sell-tickets-command (id tickets)
  (with-retries 2
    (multiple-value-bind (version aggregate)
        (rehydrate 'ticketed-event id)
      (sell-tickets aggregate tickets version))))

(defun close-ticketed-event-command (id)
  (with-retries 2
    (multiple-value-bind (version aggregate)
        (rehydrate 'ticketed-event id)
      (close-ticketed-event aggregate version))))

;; Events
(defclass event () ())
(defclass ticketed-event-created (event) ((id :initarg :id) (name :initarg :name) (tickets :initarg :tickets)))
(defclass capacity-changed (event) ((id :initarg :id) (tickets :initarg :tickets)))
(defclass tickets-sold (event) ((id :initarg :id) (quantity :initarg :quantity)))
(defclass ticketed-event-closed (event) ((id :initarg :id)))

;; Listeners
(load "listeners.lisp")
(load "sagas.lisp")


;;; Domain Objects

(defclass ticketed-event () ((id :initarg :id) (name :initarg :name :initform "") (tickets :initarg :tickets :initform 0) (sold :initarg :sold :initform 0) (status  :initform 'open)))

(defun create-ticketed-event (id name capacity)
  (raise 'nil id (make-instance 'ticketed-event-created :id id :name name :tickets capacity) 0))

(defun change-capacity (aggregate capacity version)   
  (when (> capacity 0)
    (raise aggregate (slot-value aggregate 'id) (make-instance 'capacity-changed :id (slot-value aggregate 'id) :tickets capacity) version)))

 (defun sell-tickets (aggregate quantity version)
   (if (< (+ quantity (slot-value aggregate 'sold)) (slot-value aggregate 'tickets))
       (raise aggregate (slot-value aggregate 'id) (make-instance 'tickets-sold :id (slot-value aggregate 'id) :quantity quantity) version)
       (throw 'not-enough-tickets (slot-value aggregate 'id))))

(defun close-ticketed-event (aggregate version)   
    (raise aggregate (slot-value aggregate 'id) (make-instance 'ticketed-event-closed :id (slot-value aggregate 'id)) version))

;; Event Handlers

(defmethod apply-event ((event ticketed-event-created) none)
  (make-instance 'ticketed-event
                 :id (slot-value event 'id)
                 :name  (slot-value event 'name)
                 :tickets (slot-value event 'tickets)))

(defmethod apply-event ((event capacity-changed) (aggregate ticketed-event))
  (setf
   (slot-value aggregate 'tickets)
   (slot-value event 'tickets))
  aggregate)

(defmethod apply-event ((event tickets-sold) (aggregate ticketed-event))
  (setf
   (slot-value aggregate 'sold)
   (+ (slot-value aggregate 'sold) (slot-value event 'quantity)))
  aggregate)

(defmethod apply-event ((event ticketed-event-closed) (aggregate ticketed-event))
  (setf
   (slot-value aggregate 'status)
   'closed)
  aggregate)

;;
;; Demo
;;

(redis:with-connection ()
  (red:flushall))

(defvar myid (uuid:make-v4-uuid))
(create-ticketed-event-command myid "My event" 10) 
(change-capacity-command myid 5)
(sell-tickets-command myid 2)
(close-ticketed-event-command myid)

(defun fire-command ()
  (dotimes (c 500)
    (change-capacity-command myid (random 101))))

(dotimes (c 3)
  (bordeaux-threads:make-thread #'fire-command))

(multiple-value-bind (length aggregate)
    (rehydrate 'ticketed-event myid)
  (print length)
  (print aggregate))
