(defpackage :serialize (:use :common-lisp))

(setf *print-readably* 't)

(defun get-slots (object)
  (mapcar #'sb-mop:slot-definition-name (sb-mop:class-slots (class-of object))))

(set-macro-character 
     #\{
     #'(lambda (str char)
     (declare (ignore char))
     (let ((list (read-delimited-list #\} str t)))
       (let ((type (first list))
         (list (second list)))
         (let ((class (allocate-instance (find-class type))))
           (loop for i in list do
            (setf (slot-value class (car i)) (cdr i)))
           class)))))

(defmethod print-object ((object standard-object) stream)
  (format stream "{ ~s ~s}" (type-of object)
      (loop for i in (get-slots object)
	 collect (cons i (slot-value object i)))))
