;;; Saga
;;; Listens for events and generates commands

(defmethod listener ((event ticketed-event-created))
  ;; create saga
  ;; store into redis
  )

(defmethod listener ((event tickets-sold))
  ;; load saga
  ;; manipulate
  ;; store into redis
  )

(defmethod listener ((event ticketed-event-closed))
  ;; end saga
  )

(defclass saga () ())
(defclass hot-event-saga (saga) ((count :initform 0)))
