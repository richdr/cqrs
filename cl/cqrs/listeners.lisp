
(defgeneric listener (event))

;;; Event listeners
;;; TODO clustering mechanism
;;; TODO singleton with state?

(defmethod listener ((my-event event))) ; catch all

(defmethod listener ((event capacity-changed))
  (when (= (slot-value event 'tickets) 100)
    (print "100 tickets!")
  (call-next-method)))

